from django.db import models
from graffitiUserAccounts.GraffitiUser import GraffitiUser

class GraffitiUserSettings(models.Model):
    ON = 'ON'
    OFF = 'OFF'

    ON_OR_OFF = (
            (ON, 'On'),
            (OFF, 'Off')
    )

    user = models.ForeignKey(GraffitiUser)
    reportUserLocation = models.CharField(max_length = 3, choices = ON_OR_OFF, default = ON)
    messageLikedNotice = models.CharField(max_length = 3, choices = ON_OR_OFF, default = ON)
    friendsAddingNotice = models.CharField(max_length = 3, choices = ON_OR_OFF, default = ON)
    nearbyEventNotice = models.CharField(max_length = 3, choices = ON_OR_OFF, default = ON)
