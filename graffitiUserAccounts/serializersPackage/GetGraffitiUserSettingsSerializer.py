from django.conf import settings
from rest_framework import serializers
from graffitiUserAccounts.models import GraffitiUser, GraffitiUserSettings

class GetGraffitiUserSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = GraffitiUserSettings
        fields = ('reportUserLocation', 'messageLikedNotice', 'friendsAddingNotice', 'nearbyEventNotice')
