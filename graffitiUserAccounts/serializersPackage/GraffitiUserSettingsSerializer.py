from django.conf import settings
from rest_framework import serializers
from graffitiUserAccounts.models import GraffitiUser, GraffitiUserSettings

class GraffitiUserSettingsSerializer(serializers.ModelSerializer):
    userID = serializers.IntegerField()
    class Meta:
        model = GraffitiUserSettings
        fields = ('userID', 'reportUserLocation', 'messageLikedNotice', 'friendsAddingNotice', 'nearbyEventNotice')

    def create(self, validated_data):
        userID = validated_data.pop('userID')
        user = GraffitiUser.objects.get(id = userID)
        userSettings = GraffitiUserSettings(user = user, **validated_data)
        userSettings.save()
        return userSettings
