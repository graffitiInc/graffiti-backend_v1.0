from django.conf import settings
from rest_framework import serializers
from graffitiUserAccounts.models import GraffitiUser

class GraffitiUserSerializer(serializers.ModelSerializer):
    userID = serializers.SerializerMethodField('getID')
    class Meta:
        model = GraffitiUser
        fields = ('userID', 'username', 'email', 'password', 'userType', 'headshot')
	#fields = ('email', 
	#		  'username', 
	#		  'first_name', 
	#		  'last_name', 
	#		  'password',
	#		  'user_psermissions',
	#		  'is_staff',
	#		  'is_active',
   	#		  'is_superuser',
	#		  'last_login',
	#		  'date_joined',
	#		  'userType',
	#		  'headshot',
	#		  'frineds',
	#		  )
    def create(self, validated_data):
        password = validated_data.pop('password', None)
        graffitiUser = self.Meta.model(**validated_data)
        if password is not None:
            graffitiUser.set_password(password)
            graffitiUser.save()
            return graffitiUser
        
    def update(self, graffitiUser, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                graffitiUser.set_password(value)
                graffitiUser.save()
            else:
                setattr(graffitiUser, attr, value)
                return graffitiUser

    def getID(self, object):
        return object.id
