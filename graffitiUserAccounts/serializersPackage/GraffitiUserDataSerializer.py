from django.conf import settings
from rest_framework import serializers
from graffitiUserAccounts.models import GraffitiUser

class GraffitiUserDataSerializer(serializers.ModelSerializer): 
    userID = serializers.SerializerMethodField('getID')
    headshot = serializers.SerializerMethodField('getImageURL')
    class Meta:
        model = GraffitiUser
        fields = ('userID', 'username', 'email', 'userType', 'headshot')
    
    def getID(self, object):
        return object.id

    def getImageURL(self, object):
        path = str(object.headshot)
        if path is '':
            return None
        path = path.split('/')
        imageName = path[-1]
        return settings.MEDIA_URL + 'image/headshot/' + imageName
        #return settings.MEDIA_URL + imageName
