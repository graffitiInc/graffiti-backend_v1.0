# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('graffitiUserAccounts', '0004_delete_graffitiusersettings'),
    ]

    operations = [
        migrations.CreateModel(
            name='GraffitiUserSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reportUserLocation', models.CharField(default=b'ON', max_length=3, choices=[(b'ON', b'On'), (b'OFF', b'Off')])),
                ('messageLikedNotice', models.CharField(default=b'ON', max_length=3, choices=[(b'ON', b'On'), (b'OFF', b'Off')])),
                ('friendsAddingNotice', models.CharField(default=b'ON', max_length=3, choices=[(b'ON', b'On'), (b'OFF', b'Off')])),
                ('nearbyEventNotice', models.CharField(default=b'ON', max_length=3, choices=[(b'ON', b'On'), (b'OFF', b'Off')])),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
