# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='GraffitiUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('email', models.EmailField(unique=True, max_length=225, verbose_name=b'email address')),
                ('username', models.CharField(max_length=30, null=True)),
                ('dataJoined', models.DateTimeField(auto_now_add=True)),
                ('userType', models.CharField(default=b'NU', max_length=2, choices=[(b'NU', b'noncommercialUser'), (b'CU', b'commercialUser')])),
                ('headshot', models.ImageField(null=True, upload_to=b'/home/yzhnasa/media/image/headshot')),
                ('friends', models.ManyToManyField(related_name='friends_rel_+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
