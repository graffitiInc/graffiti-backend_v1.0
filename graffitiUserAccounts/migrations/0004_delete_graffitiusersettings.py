# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('graffitiUserAccounts', '0003_graffitiusersettings'),
    ]

    operations = [
        migrations.DeleteModel(
            name='GraffitiUserSettings',
        ),
    ]
