#import pdb
#import sys
import os
from rest_framework import status
from rest_framework.decorators import api_view, parser_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.decorators import permission_classes
#from rest_framework.parsers import FileUploadParser
from django.contrib import auth
from django.contrib.sessions.models import Session
from django.views.decorators.csrf import csrf_exempt
from graffitiUserAccounts.models import GraffitiUser, GraffitiUserSettings
from graffitiUserAccounts.serializers import GraffitiUserSerializer, GraffitiUserDataSerializer, GraffitiUserSettingsSerializer, GetGraffitiUserSettingsSerializer
from graffitiUserAccounts.utilities import getHeadshotURL
from graffitiBasicFunctions.models import UserLocation
from graffitiBasicFunctions.serializers import ReportUserLocationSerializer, UserLocationSerializer


@api_view(['POST'])
#@parser_classes((FileUploadParser, ))
@permission_classes((AllowAny, ))
@csrf_exempt
def signup(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    serializer = GraffitiUserSerializer(data = request.DATA)
    if serializer.is_valid():
        user = serializer.save()
        #user = GraffitiUser.objects.get(id = serializer.data['userID'])
        if 'headshot' in request.data:
            user.headshot = request.data['headshot']
            user.save()
        return Response(status = status.HTTP_201_CREATED)
    else:
        return Response(status = status.HTTP_406_NOT_ACCEPTABLE) 


@api_view(['POST'])
@permission_classes((AllowAny, ))  
@csrf_exempt
def login(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    email = request.data['email']
    password = request.data['password']
    user = auth.authenticate(email = email, password = password)
    serializer = GraffitiUserDataSerializer(user)
    if user is not None and user.is_active:
        auth.login(request, user)
        return Response(serializer.data, status = status.HTTP_200_OK)
    else:
        return Response(status = status.HTTP_401_UNAUTHORIZED)
	

@api_view(['POST'])
@permission_classes((AllowAny, ))
@csrf_exempt
def logout(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    try:
        auth.logout(request)
    except KeyError:
        pass
    return Response(status = status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def getUserID(request, format = None):
    if request.method != 'GET':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    content = {'userID': request.user.id}
    return Response(content, status = status.HTTP_302_FOUND)


@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def getUsername(request, format = None):
    if request.method != 'GET':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    content = {'username': request.user.username}
    return Response(content, status = status.HTTP_302_FOUND)


@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def getUserEmail(request, format = None):
    if request.method != 'GET':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    content = {'email': request.user.email}
    return Response(content, status = status.HTTP_302_FOUND)


# Should have get user password for superuser. #


@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def getUserType(request, format = None):
    if request.method != 'GET':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    content = {'userType': request.user.userType}
    return Response(content, status = status.HTTP_302_FOUND)


@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def getUserHeadshotURL(request, format = None):
    if request.method != 'GET':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    user = request.user
    content = {'headshot': getHeadshotURL(user)}
    return Response(content, status = status.HTTP_302_FOUND)


@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def getUserHeadshotImage(request, format = None):
    if request.method != 'GET':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    user = request.user
    headshot = open(str(request.user.headshot), 'rb').read()
    return Response(headshot, status = status.HTTP_302_FOUND)


@api_view(['PUT'])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def changeUsername(request, username, format = None):
    if request.method != 'PUT':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    user = request.user
    user.username = username
    user.save()
    return Response(status = status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def changeEmail(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    user = request.user
    email = request.data['email']
    user.email = email
    user.save()
    return Response(status = status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def changePassword(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    user = request.user
    password = request.data['password']
    user.set_password(password)
    user.save()
    return Response(status = status.HTTP_200_OK)


@api_view(['PUT'])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def changeUserType(request, userType, format = None):
    if request.method != 'PUT':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    user = request.user
    user.userType = userType
    user.save()
    return Response(status = status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def changeHeadshot(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    user = request.user
    if user.headshot != None and user.headshot != '':
        os.remove(str(user.headshot))
    user.headshot = request.data['headshot']
    user.save()
    return Response(status = status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def modifyUserInfo(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    user = request.user
    if 'username' in request.data:
        user.username = request.data['username']
    if 'email' in request.data:
        user.email = request.data['email']
    if 'password' in request.data:
        password = request.data['password']
        user.set_password(password)
    if 'userType' in request.data:
        user.userType = request.data['userType']
    if 'headshot' in request.data:
        if user.headshot != None:
            os.remove(str(user.headshot))
        user.headshot = request.data['headshot']
    user.save()
    return Response(status = status.HTTP_200_OK)


@api_view(['DELETE'])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def deleteHeadshot(request, format = None):
    if request.method != 'DELETE':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    user = request.user
    if user.headshot == None or user.headshot == '':
        return Response(status = status.HTTP_404_NOT_FOUND)
    os.remove(str(user.headshot))
    user.headshot = None
    user.save()
    return Response(status = status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def reportUserLocation(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    user = request.user
    userID = user.id
    request.data['userID'] = userID
    serializer = ReportUserLocationSerializer(data = request.DATA)
    if serializer.is_valid():
        serializer.save()
        return Response(status = status.HTTP_201_CREATED)
    else:
        return Response(status = status.HTTP_406_NOT_ACCEPTABLE)


@api_view(['POST', 'GET', 'PUT'])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def userSetting(request, format = None):
    user = request.user
    userID = user.id
    if request.method == 'POST':
        request.data['userID'] = userID
        userSetting = GraffitiUserSettings.objects.filter(user_id = userID)
        if userSetting.exists():
            return Response(status = status.HTTP_406_NOT_ACCEPTABLE)
        serializer = GraffitiUserSettingsSerializer(data = request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(status = status.HTTP_200_OK)
        else:
            return Response(status = status.HTTP_400_BAD_REQUEST)
    if request.method == 'PUT':
        try:
            userSetting = GraffitiUserSettings.objects.get(user_id = userID)
            if 'reportUserLocation' in request.data:
                userSetting.reportUserLocation = request.data['reportUserLocation']
            if 'messageLikedNotice' in request.data:
                userSetting.messageLikedNotice = request.data['messageLikedNotice']
            if 'friendsAddingNotice' in request.data:
                userSetting.friendsAddingNotice = request.data['friendsAddingNotice']
            if 'nearbyEventNotice' in request.data:
                userSetting.nearbyEventNotice = request.data['nearbyEventNotice']
            userSetting.save()
            return Response(status = status.HTTP_200_OK)
        except GraffitiUserSettings.DoesNotExist:
            return Response(status = status.HTTP_404_NOT_FOUND)
    if request.method == 'GET':
        userSetting = GraffitiUserSettings.objects.filter(user_id = userID)
        if not userSetting.exists():
            return Response(status = status.HTTP_404_NOT_FOUND)
        userSetting = GraffitiUserSettings.objects.get(user_id = userID)
        serializer = GetGraffitiUserSettingsSerializer(userSetting)
        return Response(serializer.data, status = status.HTTP_200_OK)
    else:
        return Response(status = status.HTTP_400_BAD_REQUEST)
