#from django.contrib.auth.models import check_password
from django.core.exceptions import ObjectDoesNotExist
from graffitiUserAccounts.GraffitiUser import GraffitiUser

class GraffitiAuthentication(object):
    def authentication(self, email = None, password = None):
        try:
            user = GraffitiUser.objects.get(email = email)
        except ObjectDoesNotExist:
            return None
        passwordValid = user.check_password(password)
        if passwordValid:
            return user
	return None
    
    def get_user(self, user_id):
        try:
            user = GraffitiUser.objects.get(pk = user_id)
            if user.is_active:
                return user
            return None
        except user.ObjectDoesNotExist:
            return None
