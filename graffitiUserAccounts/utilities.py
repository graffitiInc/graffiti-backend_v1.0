from django.conf import settings

def getUserMediaPath(user, filename):
    return settings.MEDIA_ROOT + str(user.id) + '/media/' + filename

def getHeadshotPath(user, filename):
    return settings.MEDIA_ROOT + str(user.id) + '/media/image/headshot/' + filename

def getHeadshotURL(user):
    path = str(user.headshot)
    path = path.split('/')
    imageName = path[-1]
    return settings.MEDIA_URL + 'image/headshot/' + imageName


def handleUploadedFile(file, filename):
    with open((settings.MEDIA_ROOT)+str(filename), 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)
	destination.close()
