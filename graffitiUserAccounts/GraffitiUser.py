from django.db import models
from django.contrib.auth.models import AbstractBaseUser
#from graffiti import settings
from django.conf import settings
from graffitiUserAccounts.GraffitiUserManager import GraffitiUserManager
#from graffitiBasicFunctions.modelsPackage.ContactInfo import ContactInfo
#from graffitiBasicFunctions.modelsPackage.UserLocation import Location

class GraffitiUser(AbstractBaseUser):
    NONCOMMERCIAL_USER = 'NU'
    COMMERCIAL_USER = 'CU'
    USER_TYPE_CHOICES = (
        (NONCOMMERCIAL_USER, 'noncommercialUser'),
        (COMMERCIAL_USER, 'commercialUser'),
    )
    email = models.EmailField(unique = True, verbose_name = 'email address', max_length = 225, blank = False)
    username = models.CharField(max_length = 30, null = True)
    dataJoined = models.DateTimeField(auto_now_add = True)
    userType = models.CharField(max_length = 2, choices = USER_TYPE_CHOICES, default = NONCOMMERCIAL_USER)
    #contactInfo = models.OneToOneField(ContactInfo, null = True)
    headshot = models.ImageField(upload_to = settings.MEDIA_ROOT, null = True)
    friends = models.ManyToManyField("self")
    objects = GraffitiUserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELD = ['email', 'username', 'password', 'userType']
