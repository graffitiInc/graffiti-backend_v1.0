# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('graffitiBasicFunctions', '0014_auto_20150824_1724'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='message',
            name='untilTime',
        ),
    ]
