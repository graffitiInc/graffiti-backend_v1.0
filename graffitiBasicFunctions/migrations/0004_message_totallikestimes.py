# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('graffitiBasicFunctions', '0003_auto_20150715_0810'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='totalLikesTimes',
            field=models.IntegerField(default=0),
        ),
    ]
