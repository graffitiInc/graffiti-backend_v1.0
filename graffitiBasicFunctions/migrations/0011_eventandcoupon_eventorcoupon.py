# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('graffitiBasicFunctions', '0010_auto_20150813_0743'),
    ]

    operations = [
        migrations.AddField(
            model_name='eventandcoupon',
            name='eventOrCoupon',
            field=models.CharField(default=b'C', max_length=1, choices=[(b'E', b'Event'), (b'C', b'Coupon')]),
        ),
    ]
