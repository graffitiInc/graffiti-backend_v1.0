# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('graffitiBasicFunctions', '0004_message_totallikestimes'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='totalDisLikesTimes',
            field=models.IntegerField(default=0),
        ),
    ]
