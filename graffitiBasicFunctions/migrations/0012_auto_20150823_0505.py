# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import django.contrib.gis.db.models.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('graffitiBasicFunctions', '0011_eventandcoupon_eventorcoupon'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='eventandcoupon',
            name='message',
        ),
        migrations.RemoveField(
            model_name='eventandcoupon',
            name='timeStamp',
        ),
        migrations.AddField(
            model_name='eventandcoupon',
            name='audio',
            field=models.FileField(null=True, upload_to=b'/home/yzhnasa/media/'),
        ),
        migrations.AddField(
            model_name='eventandcoupon',
            name='author',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='eventandcoupon',
            name='createdTime',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True),
        ),
        migrations.AddField(
            model_name='eventandcoupon',
            name='image',
            field=models.ImageField(null=True, upload_to=b'/home/yzhnasa/media/'),
        ),
        migrations.AddField(
            model_name='eventandcoupon',
            name='location',
            field=django.contrib.gis.db.models.fields.PointField(srid=4326, geography=True, null=True, verbose_name='longitude/latitude', blank=True),
        ),
        migrations.AddField(
            model_name='eventandcoupon',
            name='text',
            field=models.CharField(max_length=1000, null=True),
        ),
        migrations.AddField(
            model_name='eventandcoupon',
            name='title',
            field=models.CharField(max_length=30, null=True),
        ),
        migrations.AddField(
            model_name='eventandcoupon',
            name='totalLikedTimes',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='eventandcoupon',
            name='totalReadTimes',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='eventandcoupon',
            name='video',
            field=models.FileField(null=True, upload_to=b'/home/yzhnasa/media/'),
        ),
        migrations.AlterField(
            model_name='eventandcoupon',
            name='untilTime',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True),
        ),
    ]
