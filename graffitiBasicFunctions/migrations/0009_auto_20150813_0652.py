# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('graffitiBasicFunctions', '0008_auto_20150716_0612'),
    ]

    operations = [
        migrations.CreateModel(
            name='UnconfirmedFriendAdding',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timeStamp', models.DateTimeField(default=datetime.datetime.now)),
                ('fromUser', models.ForeignKey(related_name='fromUser', to=settings.AUTH_USER_MODEL)),
                ('toUser', models.ForeignKey(related_name='toUser', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RemoveField(
            model_name='addfriendnotification',
            name='fromUser',
        ),
        migrations.RemoveField(
            model_name='addfriendnotification',
            name='toUser',
        ),
        migrations.DeleteModel(
            name='AddFriendNotification',
        ),
    ]
