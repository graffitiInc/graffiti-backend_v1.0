# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import django.contrib.gis.db.models.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AddFriendNotification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timeStamp', models.DateTimeField(default=datetime.datetime.now)),
                ('fromUser', models.ForeignKey(related_name='fromUser', to=settings.AUTH_USER_MODEL)),
                ('toUser', models.ForeignKey(related_name='toUser', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ContactInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phoneNumber', models.CharField(max_length=15, null=True)),
                ('address', models.CharField(max_length=100, null=True)),
                ('emailAddress', models.EmailField(max_length=254, null=True)),
                ('facebookID', models.CharField(max_length=30, null=True)),
                ('snapchatID', models.CharField(max_length=30, null=True)),
                ('instagramID', models.CharField(max_length=30, null=True)),
                ('googleID', models.CharField(max_length=30, null=True)),
                ('twitterID', models.CharField(max_length=30, null=True)),
                ('user', models.OneToOneField(null=True, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='EventNotification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timeStamp', models.DateTimeField(default=datetime.datetime.now)),
                ('timeLimit', models.IntegerField(default=5)),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=30, null=True)),
                ('anonymous', models.CharField(default=b'Y', max_length=1, choices=[(b'Y', b'Yes'), (b'N', b'No')])),
                ('messageType', models.CharField(default=b'PO', max_length=2, choices=[(b'AD', b'adverisement'), (b'CO', b'comment'), (b'CU', b'coupon'), (b'NO', b'notification'), (b'PO', b'post')])),
                ('createdTime', models.DateTimeField(default=datetime.datetime.now, blank=True)),
                ('modifiedTime', models.DateTimeField(default=datetime.datetime.now, blank=True)),
                ('location', django.contrib.gis.db.models.fields.PointField(srid=4326, geography=True, null=True, verbose_name='longitude/latitude', blank=True)),
                ('totalReadTimes', models.IntegerField()),
                ('audio', models.FileField(null=True, upload_to=b'/home/yzhnasa/media/audio')),
                ('image', models.ImageField(null=True, upload_to=b'/home/yzhnasa/media/image/messageImage')),
                ('video', models.FileField(null=True, upload_to=b'/home/yzhnasa/media/video')),
                ('text', models.CharField(max_length=1000, null=True)),
                ('author', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('comments', models.ForeignKey(to='graffitiBasicFunctions.Message', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='UserLocation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('location', django.contrib.gis.db.models.fields.PointField(srid=4326, geography=True, null=True, verbose_name='longitude/latitude', blank=True)),
                ('visitTime', models.DateTimeField(default=datetime.datetime.now, blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='eventnotification',
            name='message',
            field=models.OneToOneField(to='graffitiBasicFunctions.Message'),
        ),
    ]
