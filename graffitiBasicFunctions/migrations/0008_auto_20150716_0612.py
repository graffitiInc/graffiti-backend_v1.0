# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('graffitiBasicFunctions', '0007_auto_20150715_2057'),
    ]

    operations = [
        migrations.RenameField(
            model_name='message',
            old_name='comments',
            new_name='commentedMessage',
        ),
    ]
