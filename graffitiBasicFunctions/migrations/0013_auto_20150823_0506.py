# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('graffitiBasicFunctions', '0012_auto_20150823_0505'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventandcoupon',
            name='author',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
