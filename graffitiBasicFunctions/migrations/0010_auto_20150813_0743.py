# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('graffitiBasicFunctions', '0009_auto_20150813_0652'),
    ]

    operations = [
        migrations.CreateModel(
            name='EventAndCoupon',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timeStamp', models.DateTimeField(default=datetime.datetime.now)),
                ('untilTime', models.DateTimeField(default=datetime.datetime.now)),
                ('message', models.OneToOneField(to='graffitiBasicFunctions.Message')),
            ],
        ),
        migrations.RemoveField(
            model_name='eventnotification',
            name='message',
        ),
        migrations.DeleteModel(
            name='EventNotification',
        ),
    ]
