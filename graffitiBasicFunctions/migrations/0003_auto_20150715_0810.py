# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('graffitiBasicFunctions', '0002_auto_20150706_0620'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='audio',
            field=models.FileField(null=True, upload_to=b'/home/yzhnasa/media/'),
        ),
        migrations.AlterField(
            model_name='message',
            name='image',
            field=models.ImageField(null=True, upload_to=b'/home/yzhnasa/media/'),
        ),
        migrations.AlterField(
            model_name='message',
            name='video',
            field=models.FileField(null=True, upload_to=b'/home/yzhnasa/media/'),
        ),
    ]
