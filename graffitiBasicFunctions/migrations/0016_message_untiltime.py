# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('graffitiBasicFunctions', '0015_remove_message_untiltime'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='untilTime',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True),
        ),
    ]
