# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('graffitiBasicFunctions', '0013_auto_20150823_0506'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='eventandcoupon',
            name='author',
        ),
        migrations.AddField(
            model_name='message',
            name='untilTime',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True),
        ),
        migrations.DeleteModel(
            name='EventAndCoupon',
        ),
    ]
