# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('graffitiBasicFunctions', '0005_message_totaldislikestimes'),
    ]

    operations = [
        migrations.RenameField(
            model_name='message',
            old_name='totalDisLikesTimes',
            new_name='totalDisLikedTimes',
        ),
        migrations.RenameField(
            model_name='message',
            old_name='totalLikesTimes',
            new_name='totalLikedTimes',
        ),
    ]
