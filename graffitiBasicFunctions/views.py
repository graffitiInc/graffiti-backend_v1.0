import os
import json
from rest_framework import status
from rest_framework.decorators import api_view, parser_classes, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.parsers import FileUploadParser
from django.contrib.gis import geos
from django.contrib.gis.measure import Distance, D
from django.contrib.sessions.models import Session
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.utils.datastructures import MultiValueDict
from graffitiUserAccounts.models import GraffitiUser
from graffitiUserAccounts.serializers import GraffitiUserDataSerializer
from graffitiBasicFunctions.serializers import PostMessageSerializer, MessageSerializer, ReportUserLocationSerializer, UserLocationSerializer, UnconfirmedFriendAddingSerializer
from graffitiBasicFunctions.models import Message, UnconfirmedFriendAdding


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
#@permission_classes((AllowAny, ))
@csrf_exempt
def postNewMessage(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    user = request.user
    userID = user.id
    request.data['userID'] = userID
    print request.data
    #data = MultiValueDict(request.DATA)
    serializer = PostMessageSerializer(data = request.DATA)
    #serializer = PostMessageSerializer(data = data)
    print serializer.is_valid()
    if serializer.is_valid():
        print 'here here'
        message = serializer.save()
        if 'image' in request.data:
            message.image = request.data['image']
        if 'audio' in request.data:
            message.audio = request.data['audio']
        if 'video' in request.data:
            message.video = request.data['video']
        message.save()
        return Response(status = status.HTTP_201_CREATED)
    else:
        return Response(status = status.HTTP_406_NOT_ACCEPTABLE)


@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
#@permission_classes((AllowAny, ))
@csrf_exempt
def getUserTotalPostCount(request, format = None):
    if request.method != 'GET':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    #userID = request.data['userID']
    user = request.user
    userID = user.id
    totalCount = Message.objects.filter(author_id = userID).count()
    content = {'totalCount': totalCount}
    return Response(content, status = status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
#@permission_classes((AllowAny, ))
@csrf_exempt
def getUserMessages(request, format = None):
    if request.method != 'GET':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    #userID = request.data['userID']
    user = request.user
    userID = user.id
    limit = request.data['limit']
    offset = request.data['offset']
    messages = Message.objects.filter(author_id = userID).order_by("createdTime")[offset:limit]
    #print messages
    #messages = Message.objects.filter(author_id = userID).latest("createdTime")[offset:limit]
    serializer = MessageSerializer(messages, many = True)
    #print serializer
    if messages is not None:
        return Response(serializer.data, status = status.HTTP_200_OK)
    else:
        return Response(status = status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
#@permission_classes((AllowAny, ))
@csrf_exempt
def modifyPostedMessage(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    user = request.user
    userID = user.id
    messageID = request.data['messageID']
    message = Message.objects.get(id = messageID)
    if str(message.author_id) != str(userID):
        return Response(status = status.HTTP_403_FORBIDDEN)
    if 'title' in request.data:
        message.title = request.data['title']
    if 'anonymous' in request.data:
        message.anonymous = request.data['anonymous']
    if 'text' in request.data:
        message.text = request.data['text']
    if 'image' in request.data:
        if message.image != None and message.audio != '':
            os.remove(str(message.image))
        message.image = request.data['image']
    if 'audio' in request.data:
        if message.audio != None and message.audio != '':
            os.remove(str(message.audio))
        message.audio = request.data['audio']
    if 'video' in request.data:
        if message.video != None and message.audio != '':
            os.remove(str(message.video))
        message.video = request.data['video']
    if 'untilTime' in request.data:
        message.title = request,data['untilTime']
    message.modifiedTime = request.data['modifiedTime']
    message.save()
    return Response(status = status.HTTP_200_OK)


@api_view(['DELETE'])
@permission_classes((IsAuthenticated, ))
#@permission_classes((AllowAny, ))
@csrf_exempt
def deletePostedMessage(request, format = None):
    if request.method != 'DELETE':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    #userID = request.data['userID']
    user = request.user
    userID = user.id
    messageID = request.data['messageID']
    message = Message.objects.get(id = messageID)
    if message.audio != None and message.audio != '':
        os.remove(str(message.audio))
    if message.audio != None and message.image != '':
        os.remove(str(message.image))
    if message.audio != None and message.video != '':
        os.remove(str(message.video))
    message.delete()
    return Response(status = status.HTTP_200_OK)


@api_view(['GET'])
#@permission_classes((IsAuthenticated, ))
@permission_classes((AllowAny, ))
@csrf_exempt
def getNearbyMessage(request, format = None):
    if request.method != 'GET':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    locationData = json.loads(request.data['location'])
    point = "POINT(%s %s)" % (float(locationData[0]), float(locationData[1]))
    location = geos.fromstr(point, srid = 4326)
    distance = request.data['distance']
    limit = request.data['limit']
    offset = request.data['offset']
    #messages = Message.objects.filter(location__distance_lte=(location, Distance(m = distance))).distance(location).order_by("distance")#[offset:limit]
    messages = Message.objects.filter(location__distance_lte=(location, Distance(m = distance)), messageType = 'PO').order_by('-totalLikedTimes', '-totalReadTimes')[offset:limit]
    if messages is not None:
        serializer = MessageSerializer(messages, many = True)
        for message in messages:
            message.totalReadTimes = message.totalReadTimes + 1
            message.save()
        return Response(serializer.data, status = status.HTTP_200_OK)
    else:
        return Response(status = status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
#@permission_classes((IsAuthenticated, ))
@permission_classes((AllowAny, ))
@csrf_exempt
def getComments(request, format = None):
    if request.method != 'GET':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    messageID = request.data['messageID']
    messages = Message.objects.filter(commentedMessage_id = messageID).order_by('createdTime')
    if messages is not None:
        serializer = MessageSerializer(messages, many = True)
        for message in messages:
            message.totalReadTimes = message.totalReadTimes + 1
            message.save()
        return Response(serializer.data, status = status.HTTP_200_OK)
    else:
        return Response(status = status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
#@permission_classes((IsAuthenticated, ))
@permission_classes((AllowAny, ))
@csrf_exempt
def likeAMessage(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    messageID = request.data['messageID']
    message = Message.objects.get(id = messageID)
    if message is None:
        return Response(status = status.HTTP_404_NOT_FOUND)
    message.totalLikedTimes = message.totalLikedTimes + 1
    message.save()
    return Response(status = status.HTTP_200_OK)


@api_view(['POST'])
#@permission_classes((IsAuthenticated, ))
@permission_classes((AllowAny, ))
@csrf_exempt
def cancelLikeAMessage(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    messageID = request.data['messageID']
    message = Message.objects.get(id = messageID)
    if message is None:
        return Response(status = status.HTTP_404_NOT_FOUND)
    message.totalLikedTimes = message.totalLikedTimes - 1
    message.save()
    return Response(status = status.HTTP_200_OK)


@api_view(['POST'])
#@permission_classes((IsAuthenticated, ))
@permission_classes((AllowAny, ))
@csrf_exempt
def dislikeAMessage(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    messageID = request.data['messageID']
    message = Message.objects.get(id = messageID)
    if message is None:
        return Response(status = status.HTTP_404_NOT_FOUND)
    message.totalDislikedTimes = message.totalDislikedTimes + 1
    message.save()
    return Response(status = status.HTTP_200_OK)


@api_view(['POST'])
#@permission_classes((IsAuthenticated, ))
@permission_classes((AllowAny, ))
@csrf_exempt
def cancelDislikeAMessage(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    messageID = request.data['messageID']
    message = Message.objects.get(id = messageID)
    if message is None:
        return Response(status = status.HTTP_404_NOT_FOUND)
    message.totalDislikedTimes = message.totalDislikedTimes - 1
    message.save()
    return Response(status = status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
#@permission_classes((AllowAny, ))
@csrf_exempt
def reportUserLocation(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    user = request.user
    userID = user.id
    request.data['userID'] = userID
    serializer = ReportUserLocationSerializer(data = request.DATA)
    if serializer.is_valid():
        userLocation = serializer.save()
        return Response(status = status.HTTP_201_CREATED)
    else:
        return Response(status = status.HTTP_406_NOT_ACCEPTABLE)


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
#@permission_classes((AllowAny, ))
@csrf_exempt
def addingFriend(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    fromUser = request.user
    #fromUserID = user.id
    toUserID = request.data['toUserID']
    toUser = GraffitiUser.objects.get(id = toUserID)
    unconfirmedFriendAdding = UnconfirmedFriendAdding(fromUser = fromUser, toUser = toUser)
    unconfirmedFriendAdding.save()
    return Response(status = status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
#@permission_classes((AllowAny, ))
@csrf_exempt
def ignoreAddingFriend(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    toUser = request.user
    #toUserID = request.data['toUserID']
    #toUser = GraffitiUser.objects.get(id = toUserID)
    if 'fromUserID' in request.data:
        fromUserID = request.data['fromUserID']
        fromUser = GraffitiUser.objects.get(id = fromUserID) 
        unconfirmedFriendAdding = UnconfirmedFriendAdding.objects.filter(fromUser = fromUser, toUser = toUser)
    if 'unconfirmedFriendAddingID' in request.data:
        unconfirmedFriendAddingID = request.data['unconfirmedFriendAddingID']
        unconfirmedFriendAdding = UnconfirmedFriendAdding.objects.get(id = unconfirmedFriendAddingID)
    unconfirmedFriendAdding.delete()
    return Response(status = status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
#@permission_classes((AllowAny, ))
@csrf_exempt
def confirmAddingFriend(request, format = None):
    if request.method != 'POST':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    toUser = request.user
    #fromUserID = user.id
    #toUserID = request.data['toUserID']
    #toUser = GraffitiUser.objects.get(id = toUserID)
    if 'fromUserID' in request.data:
        fromUserID = request.data['fromUserID']
        fromUser = GraffitiUser.objects.get(id = fromUserID)
        unconfirmedFriendAdding = UnconfirmedFriendAdding.objects.filter(fromUser = fromUser, toUser = toUser)
    elif 'unconfirmedFriendAddingID' in request.data:
        unconfirmedFriendAddingID = request.data['unconfirmedFriendAddingID']
        unconfirmedFriendAdding = UnconfirmedFriendAdding.objects.get(id = unconfirmedFriendAddingID)
        toUser = unconfirmedFriendAdding.toUser
        fromUser = unconfirmedFriendAdding.fromUser
    else:
        return Response(status = status.HTTP_406_NOT_ACCEPTABLE)
    fromUser.friends.add(toUser)
    fromUser.save()
    toUser.friends.add(fromUser)
    toUser.save()
    unconfirmedFriendAdding.delete()
    return Response(status = status.HTTP_200_OK)

@api_view(['DELETE'])
@permission_classes((IsAuthenticated, ))
#@permission_classes((AllowAny, ))
@csrf_exempt
def deleteFriend(request, format = None):
    if request.method != 'DELETE':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    user = request.user
    friendID = request.data['friendID']
    friend = GraffitiUser.objects.get(id = friendID)
    user.friends.remove(friend)
    friend.friends.remove(user)
    return Response(status = status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
#@permission_classes((AllowAny, ))
@csrf_exempt
def getAddingFriendNotification(request, format = None):
    if request.method != 'GET':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    toUser = request.user
    unconfirmedFriendAdding = UnconfirmedFriendAdding.objects.filter(toUser = toUser)
    if unconfirmedFriendAdding is None:
        return Response(status = status.HTTP_404_NOT_FOUND)
    else:
        serializer = UnconfirmedFriendAddingSerializer(unconfirmedFriendAdding, many = True)
        return Response(serializer.data, status = status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
#@permission_classes((AllowAny, ))
@csrf_exempt
def getFriendsList(request, format = None):
    if request.method != 'GET':
        return Response(status = status.HTTP_400_BAD_REQUEST)
    user = request.user
    friends = user.friends.all()
    if friends is not None:
        serializer = GraffitiUserDataSerializer(friends, many = True)
        return Response(serializer.data, status = status.HTTP_200_OK)
    else:
        return Response(status = status.HTTP_404_NOT_FOUND)
