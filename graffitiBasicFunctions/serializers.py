#from graffitiBasicFunctions.serializersPackage.MessageSerializer import *
#from graffitiBasicFunctions.serializersPackage import LocationSerializer
import json
from rest_framework import serializers
from rest_framework_gis.serializers import GeoModelSerializer
from django.conf import settings
from django.contrib.gis import geos
from graffitiUserAccounts.GraffitiUser import GraffitiUser
from graffitiUserAccounts.serializers import GraffitiUserDataSerializer
from graffitiBasicFunctions.models import Message, UserLocation, UnconfirmedFriendAdding


class PostMessageSerializer(GeoModelSerializer):
    userID = serializers.IntegerField()
    location = serializers.CharField()
    commentedMessageID = serializers.IntegerField(required = False)
    untilTime = serializers.DateTimeField(required = False)
    class Meta:
        model = Message
        fields = ('userID',
                  'anonymous',
                  'location',
                  'title',
                  'messageType',
                  'text',
                  'image',
                  'audio',
                  'video',
                  'createdTime',
                  'modifiedTime',
                  'untilTime',
                  'commentedMessageID')
        geo_field = 'location'
    
    def create(self, validated_data):
        userID = validated_data.pop('userID')
        user = GraffitiUser.objects.get(id = userID)
        locationData = json.loads(validated_data.pop('location'))
        point = "POINT(%s %s)" % (float(locationData[0]), float(locationData[1]))
        location = geos.fromstr(point)
        if 'commentedMessageID' in validated_data:
            commentedMessageID = validated_data.pop('commentedMessageID')
            commentedMessage = Message.objects.get(id = commentedMessageID)
            message = Message(author = user, location = location, commentedMessage = commentedMessage, **validated_data)
        else:
            message = Message(author = user, location = location, **validated_data)
        message.save()
        return message


class MessageSerializer(GeoModelSerializer):
    messageID = serializers.SerializerMethodField('getMessageID')
    image = serializers.SerializerMethodField('getImageURL')
    audio = serializers.SerializerMethodField('getAudioURL')
    video = serializers.SerializerMethodField('getVideoURL')
    commentedMessageID = serializers.SerializerMethodField('getCommentedMessageID')
    class Meta:
        model = Message
        fields = ('messageID',
                  'author',
                  'anonymous',
                  'location',
                  'title',
                  'messageType',
                  'text',
                  'image',
                  'audio',
                  'video',
                  'createdTime',
                  'totalReadTimes',
                  'totalLikedTimes',
                  'totalDislikedTimes',
                  'modifiedTime',
                  'untilTime',
                  'commentedMessageID')
        geo_field = 'location'

    def getMessageID(self, object):
        return object.id

    def getImageURL(self, object):
        path = str(object.image)
        if path is '':
            return None
        path = path.split('/')
        imageName = path[-1]
        return settings.MEDIA_URL + 'message/image/' + imageName

    def getAudioURL(self, object):
        path = str(object.audio)
        if path is '':
            return None
        path = path.split('/')
        audioName = path[-1]
        return settings.MEDIA_URL + 'message/audio/' + audioName

    def getVideoURL(self, object):
        path = str(object.video)
        if path is '':
            return None
        path = path.split('/')
        videoName = path[-1]
        return settings.MEDIA_URL + 'message/video/' + videoName

    def getCommentedMessageID(self, object):
        return object.commentedMessage_id

#class PostEventAndCouponSerializer(GeoModelSerializer):
    #userID = serializers.IntegerField()
    #location = serializers.CharField()
    #class Meta:
        #model = EventAndCoupon
        #fields = ('userID',
                  #'location',
                  #'title',
                  #'text',
                  #'image',
                  #'audio',
                  #'video',
                  #'createdTime',
                  #'untilTime',
		  #'eventOrCoupon')
        #geo_field = 'location'
   # 
    #def create(self, validated_data):
        #userID = validated_data.pop('userID')
        #user = GraffitiUser.objects.get(id = userID)
        #locationData = json.loads(validated_data.pop('location'))
        #point = "POINT(%s %s)" % (float(locationData[0]), float(locationData[1]))
        #location = geos.fromstr(point)
        #message = Message(author = user, location = location, **validated_data)
        #message.save()
        #return message
#
#
#class EventAndCouponSerializer(GeoModelSerializer):
    #eventAndCouponID = serializers.SerializerMethodField('getEventAndCouponID')
    #image = serializers.SerializerMethodField('getImageURL')
    #audio = serializers.SerializerMethodField('getAudioURL')
    #video = serializers.SerializerMethodField('getVideoURL')
    #class Meta:
        #model = EventAndCoupon
        #fields = ('eventAndCouponID',
                  #'author',
		  #'eventOrCoupon',
                  #'location',
                  #'title',
                  #'text',
                  #'image',
                  #'audio',
                  #'video',
                  #'createdTime',
		  #'untilTime',
                  #'totalReadTimes',
                  #'totalLikedTimes')
        #geo_field = 'location'
#
    #def getEventAndCouponID(self, object):
        #return object.id
#
    #def getImageURL(self, object):
        #path = str(object.image)
        #if path is '':
            #return None
        #path = path.split('/')
        #imageName = path[-1]
        #return settings.MEDIA_URL + 'message/image/' + imageName
#
    #def getAudioURL(self, object):
        #path = str(object.audio)
        #if path is '':
            #return None
        #path = path.split('/')
        #audioName = path[-1]
        #return settings.MEDIA_URL + 'message/audio/' + audioName
#
    #def getVideoURL(self, object):
        #path = str(object.video)
        #if path is '':
            #return None
        #path = path.split('/')
        #videoName = path[-1]
        #return settings.MEDIA_URL + 'message/video/' + videoName


class ReportUserLocationSerializer(GeoModelSerializer):
    userID = serializers.IntegerField()
    location = serializers.CharField()
    class Meta:
        model = UserLocation
        fields = ('userID', 'location', 'visitTime')
        geo_field = 'location'

    def create(self, validated_data):
        userID = validated_data.pop('userID')
        user = GraffitiUser.objects.get(id = userID)
        locationData = json.loads(validated_data.pop('location'))
        point = "POINT(%s %s)" % (float(locationData[0]), float(locationData[1]))
        location = geos.fromstr(point)
        userLocation = UserLocation(user = user, location = location, **validated_data)
        userLocation.save()
        return userLocation


class UserLocationSerializer(GeoModelSerializer):
    locationID = serializers.SerializerMethodField('getUserLocationID')
    userID = serializers.IntegerField()
    location = serializers.CharField()
    class Meta:
        model = UserLocation
        fields = ('locationID', 'userID', 'location', 'visitTime')
        geo_field = 'location'

    def getUserLocationID(self, object):
        return object.id


class UnconfirmedFriendAddingSerializer(serializers.ModelSerializer):
    unconfirmedFriendAddingID = serializers.SerializerMethodField('getUnconfirmedFriendAddingID')
    class Meta:
        model = UnconfirmedFriendAdding
        fields = ('unconfirmedFriendAddingID', 'fromUser', 'toUser')
        
    def getUnconfirmedFriendAddingID(self, object):
        return object.id

