from celery import Celery
import datetime
import logging
from graffitiBasicFunctions.models import Message

app = Celery('graffiti')
#celery.config_from_object('celeryconfig')

#@celery.task
@app.task
def removeExpiredCouponAndEvent():
    #logger = logging.getLogger('task')
    #fileHandler = logging.FileHandler('/home/yzhnasa/projects/graffiti/testAPI/logger.log')
    #logger.addHandler(fileHandler)
    #logger.info('running task!')
    logging.basicConfig(filename='logger.log', level=logging.INFO)
    #logging.info('running task!')
    messages = Message.objects.select_related().all()
    #logging.info(messages)
    for message in messages:
        logging.info('removed message id: %d', message.id)
        #logging.info(message.untilTime)
        #logging.info(datetime.datetime.now())
        #result = message.untilTime < datetime.datetime.now()
        #logging.info(result)
        if message.untilTime < datetime.datetime.now():
            if message.audio != None and message.audio != '':
                os.remove(str(message.audio))
            if message.audio != None and message.image != '':
                os.remove(str(message.image))
            if message.audio != None and message.video != '':
                os.remove(str(message.video))
            message.delete()

