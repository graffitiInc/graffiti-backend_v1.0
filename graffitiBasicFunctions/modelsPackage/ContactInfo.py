from django.db import models
from graffitiUserAccounts.GraffitiUser import GraffitiUser

class ContactInfo(models.Model):
    user = models.OneToOneField(GraffitiUser, null = True)
    phoneNumber = models.CharField(max_length = 15, null = True)
    address = models.CharField(max_length = 100, null = True)
    emailAddress = models.EmailField(null = True)
    facebookID = models.CharField(max_length = 30, null = True)
    snapchatID = models.CharField(max_length = 30, null = True)
    instagramID = models.CharField(max_length = 30, null = True)
    googleID = models.CharField(max_length = 30, null = True)
    twitterID = models.CharField(max_length = 30, null = True)
