from django.db import models
from django.contrib.gis import geos
from django.contrib.gis.db import models as gisModels
from datetime import datetime
from graffitiUserAccounts.GraffitiUser import GraffitiUser
#from graffitiBasicFunctions.modelsPackage.Location import Location

class UserLocation(models.Model):
    user = models.ForeignKey(GraffitiUser)
    #location = models.ManyToManyField(Location)
    location = gisModels.PointField(u"longitude/latitude", geography = True, blank = True, null = True)
    visitTime = models.DateTimeField(default=datetime.now, blank=True)

    gis = gisModels.GeoManager()
    objects = models.Manager()

    def save(self, **kwargs):
        if not self.location:
            point = "POINT(%s %s)" % (kwargs[0], kwargs[1])
            self.location = geos.fromstr(point)
	super(UserLocation, self).save()
