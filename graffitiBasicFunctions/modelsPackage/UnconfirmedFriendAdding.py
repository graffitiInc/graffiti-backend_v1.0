from django.db import models
from datetime import datetime
from graffitiUserAccounts.GraffitiUser import GraffitiUser

class UnconfirmedFriendAdding(models.Model):
	fromUser = models.ForeignKey(GraffitiUser, related_name = "fromUser")
	toUser = models.ForeignKey(GraffitiUser, related_name = "toUser")
	timeStamp = models.DateTimeField(default = datetime.now)
