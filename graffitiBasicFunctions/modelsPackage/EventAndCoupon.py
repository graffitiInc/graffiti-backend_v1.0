from django.conf import settings
from django.contrib.gis import geos
from django.contrib.gis.db import models as gisModels
from django.db import models
from datetime import datetime
#from graffitiBasicFunctions.modelsPackage.Message import Message
from graffitiUserAccounts.GraffitiUser import GraffitiUser

class EventAndCoupon(models.Model):
    EVENT = 'E'
    COUPON = 'C'
    
    EVENT_OR_COUPON = (
            (EVENT, 'Event'),
            (COUPON, 'Coupon')
    )
    title = models.CharField(max_length = 30, null = True)
    author = models.ForeignKey(GraffitiUser)
    location = gisModels.PointField(u"longitude/latitude", geography = True, blank = True, null = True, srid = 4326)
    totalReadTimes = models.IntegerField(default = 0)
    totalLikedTimes = models.IntegerField(default = 0)
    createdTime = models.DateTimeField(default = datetime.now, blank = True)
    #timeLimit = models.IntegerField(default = 5)
    untilTime = models.DateTimeField(default = datetime.now, blank = True)
    eventOrCoupon = models.CharField(max_length = 1, choices = EVENT_OR_COUPON, default = COUPON)
    #message = models.OneToOneField(Message)
    audio = models.FileField(upload_to = settings.MEDIA_ROOT, null = True)
    image = models.ImageField(upload_to = settings.MEDIA_ROOT, null = True)
    video = models.FileField(upload_to = settings.MEDIA_ROOT, null = True)
    text = models.CharField(max_length = 1000, null = True)

    gis = gisModels.GeoManager()
    objects = models.Manager()
