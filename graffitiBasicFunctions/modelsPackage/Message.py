from django.conf import settings
from django.contrib.gis import geos
from django.contrib.gis.db import models as gisModels
from django.db import models
from datetime import datetime
from graffitiUserAccounts.GraffitiUser import GraffitiUser

class Message(models.Model):
    YES = 'Y'
    NO = 'N'

    ANSWER = (
		(YES, 'Yes'),
		(NO, 'No')
    )
    ADVERTISEMENT_TYPE = 'AD'	# Have timeliness
    COMMENT_TYPE = 'CO'
    COUPON_TYPE = 'CU'		# Have timeliness
    NOTIFICATION_TYPE = 'NO'	# Have timeliness
    POST_TYPE = 'PO'
	
    MESSAGE_TYPE_CHOICES = (
		(ADVERTISEMENT_TYPE, 'adverisement'),
		(COMMENT_TYPE, 'comment'),
		(COUPON_TYPE, 'coupon'),
		(NOTIFICATION_TYPE, 'notification'),
		(POST_TYPE, 'post'),
    )

    title = models.CharField(max_length = 30, null = True)
    anonymous = models.CharField(max_length = 1, choices = ANSWER, default = YES)
    author = models.ForeignKey(GraffitiUser)
    #userID = models.IntegerField()
    messageType = models.CharField(max_length = 2, choices = MESSAGE_TYPE_CHOICES, default = POST_TYPE)
    createdTime = models.DateTimeField(default = datetime.now, blank = True)
    modifiedTime = models.DateTimeField(default = datetime.now, blank = True)
    untilTime = models.DateTimeField(default = datetime.now, blank = True) # For Coupon and Event
    location = gisModels.PointField(u"longitude/latitude", geography = True, blank = True, null = True, srid = 4326) 
    totalReadTimes = models.IntegerField(default = 0)
    totalLikedTimes = models.IntegerField(default = 0)
    totalDislikedTimes = models.IntegerField(default = 0)
    #audio = models.FileField(upload_to = settings.MEDIA_ROOT + 'audio', null = True)
    audio = models.FileField(upload_to = settings.MEDIA_ROOT, null = True)
    #image = models.ImageField(upload_to = settings.MEDIA_ROOT + 'image/messageImage', null = True)
    image = models.ImageField(upload_to = settings.MEDIA_ROOT, null = True)
    #video = models.FileField(upload_to = settings.MEDIA_ROOT + 'video', null = True)
    video = models.FileField(upload_to = settings.MEDIA_ROOT, null = True)
    text = models.CharField(max_length = 1000, null = True)
    commentedMessage = models.ForeignKey("self", null = True)

    gis = gisModels.GeoManager()
    objects = models.Manager()
	
