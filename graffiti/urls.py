import pdb
from django.conf.urls import include, url, patterns
from django.views.decorators.csrf import csrf_exempt
from django.contrib import admin
from django.conf import settings
from rest_framework import routers, serializers, viewsets
from rest_framework.urlpatterns import format_suffix_patterns


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^account/signup/$', 'graffitiUserAccounts.views.signup'),
    url(r'^account/login/$', 'graffitiUserAccounts.views.login'),
    url(r'^account/logout/$', 'graffitiUserAccounts.views.logout'),
    #url(r'^getid/email=(?P<email>\w+|[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})/$', 'graffitiUserAccounts.views.get_user_id'),
    url(r'^account/getid/$', 'graffitiUserAccounts.views.getUserID'),
    url(r'^account/getUsername/$', 'graffitiUserAccounts.views.getUsername'),
    url(r'^account/getUserEmail/$', 'graffitiUserAccounts.views.getUserEmail'),
    url(r'^account/getUserType/$', 'graffitiUserAccounts.views.getUserType'),
    url(r'^account/getUserHeadshotURL/$', 'graffitiUserAccounts.views.getUserHeadshotURL'),
    url(r'^account/getUserHeadshotImage/$', 'graffitiUserAccounts.views.getUserHeadshotImage'), 
    url(r'^account/changeUsername/username=(?P<username>\w+)$', 'graffitiUserAccounts.views.changeUsername'),
    url(r'^account/changeEmail/$', 'graffitiUserAccounts.views.changeEmail'),
    url(r'^account/changePassword/$', 'graffitiUserAccounts.views.changePassword'),
    url(r'^account/changeUserType/userType=(?P<userType>[A-Z]{2})$', 'graffitiUserAccounts.views.changeUserType'),
    url(r'^account/changeHeadshot/$', 'graffitiUserAccounts.views.changeHeadshot'),
    url(r'^account/modifyUserInfo/$', 'graffitiUserAccounts.views.modifyUserInfo'),
    url(r'^account/deleteHeadshot/$', 'graffitiUserAccounts.views.deleteHeadshot'),
    url(r'^account/reportUserLocation/$', 'graffitiUserAccounts.views.reportUserLocation'),
    url(r'^account/userSetting/$', 'graffitiUserAccounts.views.userSetting'),
    url(r'^media/headshot/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT + 'image/headshot'}),
    url(r'^media/image/headshot/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^basicFunctions/postNewMessage/$', 'graffitiBasicFunctions.views.postNewMessage'),
    url(r'^basicFunctions/getUserTotalPostCount/$', 'graffitiBasicFunctions.views.getUserTotalPostCount'),
    url(r'^basicFunctions/getUserMessages/$', 'graffitiBasicFunctions.views.getUserMessages'),
    url(r'^basicFunctions/modifyPostedMessage/$', 'graffitiBasicFunctions.views.modifyPostedMessage'),
    url(r'^basicFunctions/deletePostedMessage/$', 'graffitiBasicFunctions.views.deletePostedmessage'),
    url(r'^basicFunctions/getNearbyMessage/$', 'graffitiBasicFunctions.views.getNearbyMessage'),
    url(r'^basicFunctions/getComments/$', 'graffitiBasicFunctions.views.getComments'),
    url(r'^basicFunctions/likeAMessage/$', 'graffitiBasicFunctions.views.likeAMessage'),
    url(r'^basicFunctions/cancelLikeAMessage/$', 'graffitiBasicFunctions.views.cancelLikeAMessage'),
    url(r'^basicFunctions/dislikeAMessage/$', 'graffitiBasicFunctions.views.dislikeAMessage'),
    url(r'^basicFunctions/cancelDislikeAMessage/$', 'graffitiBasicFunctions.views.cancelDislikeAMessage'),
    url(r'^basicFunctions/reportUserLocation/$', 'graffitiBasicFunctions.views.reportUserLocation'),
    url(r'^basicFunctions/addingFriend/$', 'graffitiBasicFunctions.views.addingFriend'),
    url(r'^basicFunctions/ignoreAddingFriend/$', 'graffitiBasicFunctions.views.ignoreAddingFriend'),
    url(r'^basicFunctions/confirmAddingFriend/$', 'graffitiBasicFunctions.views.confirmAddingFriend'),
    url(r'^basicFunctions/deleteFriend/$', 'graffitiBasicFunctions.views.deleteFriend'),
    url(r'^basicFunctions/getAddingFriendNotification/$', 'graffitiBasicFunctions.views.getAddingFriendNotification'),
    url(r'^basicFunctions/getFriendsList/$', 'graffitiBasicFunctions.views.getFriendsList'),
    url(r'^media/message/audio/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^media/message/image/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^media/message/video/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
)

